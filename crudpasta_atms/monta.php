<!DOCTYPE html>
<html lang="en">

<head>
  <title>Site PHP</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  <?php
  	$banco = "db/";
    $pasta = ['nome','email','data','mensagem'];
    if(isset($_GET['codigo'])){
      foreach($pasta as $pasta_item){
        $dados[] = file_get_contents("$banco{$_GET['codigo']}/$pasta_item.txt");
      }
    }
  ?>
  <form method="post" action="update.php">
    <label for="codigo">Código:</label>
    <input type="number" id="codigo" name="codigo" placeholder="<?php echo $_GET['codigo']; ?>"
      value="<?php echo $_GET['codigo']; ?>" required><br>
    <label for="nome">Nome:</label>
    <input type="text" id="nome" name="nome" placeholder="<?php echo $dados[0]; ?>" value="<?php echo $dados[0]; ?>"
      required><br>
    <label for="email">Email:</label>
    <input type="text" id="email" name="email" placeholder="<?php echo $dados[1]; ?>" value="<?php echo $dados[1]; ?>"
      required><br>
    <label for="data">Data:</label>
    <input type="text" id="data" name="data" placeholder="<?php echo $dados[2]; ?>" value="<?php echo $dados[2]; ?>"
      required><br>
    <label for="mensagem">Mensagem:</label>
    <textarea type="text" id="mensagem" name="mensagem" rows="10" cols="40"><?php echo $dados[3]; ?></textarea><br>
    <input type="submit" value="Enviar">
  </form>
  <br><br><a href="index.php">Voltar</a>
</body>

</html>