<?php
$banco = "db/";
$pasta = ['nome','email','data','mensagem'];
$tmp = 1;
$conjunto = 0;

echo "<CENTER>Registro cadastrados na base de dados.<br></CENTER>";
echo "<br>";
if (count(glob("$banco*", GLOB_ONLYDIR)) > 0){
  echo "<center><table border=1>";
  date_default_timezone_set('America/Sao_Paulo');
  echo "<meta charset='UTF-8'>";
  echo "<tr><th>Código</th><th>Nome</th><th>Email</th><th>Data</th><th>Mensagem</th><th>Ações</th></tr>";
}else{
  echo "<br><br><p align=center>Ainda não há nenhum registro!</p>";
}

while ($tmp <= count(glob("$banco*", GLOB_ONLYDIR))){
  $conjunto++;
  if (count(glob("$banco$tmp/*")) != 0){
      echo "<tr><td>$tmp</td>";
      foreach($pasta as $pasta_item){
        if ($pasta_item != 'mensagem'){
          echo "<td>" . file_get_contents("$banco$tmp/$pasta_item.txt") . "</td>";
        }else{
          echo "<td>";
          $mensagem = explode("\n", file_get_contents("$banco$tmp/$pasta_item.txt"));
          foreach($mensagem as $mensagem_linha){
            echo $mensagem_linha . "<br>";
          }
          echo "</td>";
        }
      }
      echo "<td><a href='delete.php?codigo=$conjunto'><img src='imgs/delete_crud.png' alt='Deletar' title='Deletar registro'></a><a href='monta.php?codigo=$conjunto'><img src='imgs/update_crud.png' alt='Atualizar' title='Atualizar registro'><a href='index.php'><img src='imgs/insert_crud.png' alt='Inserir' title='Inserir registro'></td>";
      echo "</tr>";
    }
    $tmp++;
}
echo "</table>";
?>