<?php
$banco = "../banco.txt";
if(isset($_POST['codigo'])){
  if (file_exists($banco) && !empty(file_get_contents($banco))){
    $lista = explode("\n", file_get_contents($banco));
    $conjunto = 1;
    $contador = 0;
    $lista_itens = count($lista);
    while ($contador < count($lista)){
      if ("#" == $lista[$contador]){
        if ($conjunto == $_POST['codigo']){
          $lista[$contador+1] = $_POST['nome'];
          $lista[$contador+2] = $_POST['email'];
          $lista[$contador+3] = $_POST['data'];
          $lista[$contador+4] = $_POST['mensagem'];
          $contador += 5;
          while ("#" != $lista[$contador] and $contador <= $lista_itens) {
            unset($lista[$contador]);
            $contador += 1;
          }
          break;
        }
        $conjunto += 1;
      }
      $contador += 1;
    }
    $contador = 0;
    $texto = "";
    while ($contador < $lista_itens-1){
      if (isset($lista[$contador])){
        $texto .= $lista[$contador] . "\n";
      }
      $contador += 1;
    }
    unlink($banco);
    $criar = fopen($banco, "a+");
    fwrite($criar, $texto);
    fclose($criar);
    header('Location: /crudtxt/');
  }
}
?>
