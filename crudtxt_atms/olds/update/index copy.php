<!DOCTYPE html>
<html lang="en">
<head>
	<title>Site PHP</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <?php
	$banco = "../banco.txt";
  if(isset($_GET['codigo'])){
    if (file_exists($banco) && !empty(file_get_contents($banco))){
      $lista = explode("\n", file_get_contents($banco));
      $conjunto = 1;
      $contador = 0;
      while ($contador < count($lista)){
        if ("#" == $lista[$contador]){
          if ($conjunto == $_GET['codigo']){
            $nome = $lista[$contador+1];
            $email = $lista[$contador+2];
            $data = $lista[$contador+3];
            $mensagem = $lista[$contador+4];
						$contador += 5;
						if (!isset($mensagem[1]) and "#" != $lista[$contador] and $contador <= count($lista)-1){
							$mensagem .= "\n\n";
						}
	          while ("#" != $lista[$contador] and $contador <= count($lista)-1) {
							$mensagem .= $lista[$contador];
	            $contador += 1;
	          }
            break;
          }
          $conjunto += 1;
        }
        $contador += 1;
      }
    }
  }
  ?>
  <form method="post" action="update.php">
    <label for="codigo">Código:</label>
    <input type="number" id="codigo" name="codigo" placeholder="<?php echo $_GET['codigo'];?>" value="<?php echo $_GET['codigo'];?>"required><br>
    <label for="nome">Nome:</label>
    <input type="text" id="nome" name="nome" placeholder="<?php echo $nome;?>" value="<?php echo $nome;?>" required><br>
    <label for="email">Email:</label>
    <input type="text" id="email" name="email" placeholder="<?php echo $email;?>" value="<?php echo $email;?>" required><br>
    <label for="data">Data:</label>
    <input type="text" id="data" name="data" placeholder="<?php echo $data;?>" value="<?php echo $data;?>" required><br>
    <label for="mensagem">Mensagem:</label>
    <textarea type="text" id="mensagem" name="mensagem" rows="10" cols="40"><?php echo $mensagem;?></textarea><br>
    <input type="submit" value="Enviar">
  </form>
	<br><br><a href="..">Voltar</a>
</body>
</html>
