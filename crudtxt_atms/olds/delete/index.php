<!DOCTYPE html>
<html lang="en">
<head>
	<title>Site PHP</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<form method="get" target="_self">
		<label for="codigo">Escolha o código de um conjunto de dados:</label>
		<input type="number" id="codigo" name="codigo" required><br>
		<input type="submit" value="Enviar">
	</form>
	<table>
			<tr>
					<td class="options"><button onclick="location.href = '/crudtxt/insert'">Inserir</button></td>
					<td class="options"><button onclick="location.href = '/crudtxt/update'">Atualizar</button></td>
					<td class="options"><button onclick="location.href = '/crudtxt/limpar.php'">Limpar Banco!</button></td>
					<td class="options"><button onclick="location.href = '/crudtxt/reset.php'">Resetar Banco!</button></td>
			</tr>
	</table>
	<br>
	<?php
	$banco = "../banco.txt";
  if (file_exists($banco) && !empty(file_get_contents($banco))){
    $lista = explode("\n", file_get_contents($banco));
    $conjunto = 1; //variável para continuar a ordem de aparição do "índice", neste exemplo foi usado o '#'

	echo "<table>";
	echo "<tr><th>Código</th><th>Nome</th><th>Email</th><th>Data</th><th>Mensagem</th>";
	
	foreach($lista as $lista_item){
		if ("#" == $lista_item){
			echo "</tr><tr>";
			echo "<td>$conjunto</td>";
			$conjunto += 1;
		}else{
			echo "<td>$lista_item</td>";
		}
	}
	echo "</tr></table>";
    if (isset($_GET['codigo'])){
	    $conjunto = 1; //variável para guardar a ordem de aparição do "índice", neste exemplo foi usado o '#'
	    $contador = 0; //variável temporária para manipulação do while e do array $lista
			$lista_itens = count($lista); //gravando quantos itens a lista tinha antes dos unsets

      while ($contador < count($lista)){ //vai percorrer todo o array que foi criado com todas as linhas do arquivo
				if ("#" == $lista[$contador]){ //se achar um '#' no começo da linha, valida se o conjunto é qual o usuário escolheu e acrescenta mais um à variável.
	        if ($conjunto == $_GET['codigo']){
						unset($lista[$contador]);
						while ("#" != $lista[$contador] and $contador != $lista_itens) {
							unset($lista[$contador]);
							$contador += 1;
						} //unsets para remover os elementos da lista que foi formada com as linhas do arquivo
	          break; // esta linha é equilvalente à um 'break;'
	        }
	        $conjunto += 1;
	      }
	      $contador += 1;
      }

      $contador = 0; //variável temporária para manipulação de array e da lista $lista
      $texto = ""; //futuro novo texto que estará no arquivo.
      while ($contador < $lista_itens-1){ //-1 para não colocar line breaks a mais no texto para cada execução
				if (isset($lista[$contador])){ //se não fez parte dos unsets do bloco de código anterior, será atribuído à variável $texto
	        $texto .= $lista[$contador] . "\n"; //o elemento válido será juntado à um linebreak na variável
				}
        $contador += 1;
      }
      unlink($banco); //apaga o arquivo do diretório
      $criar = fopen($banco, "a+"); //cria um novo com o mesmo nome já com a permissão de escrita ("a+")
      fwrite($criar, $texto); //escreve no arquivo criado exatamente o que foi colocado na variável $texto
      fclose($criar); //"fecha" o arquivo para o apache
      header('Location: /crudtxt/insert'); //volta para a página de delete sem a atribuição do $_GET na URL;
    }
  }else{
    echo "<br><br><p align=center>Ainda não há nenhum registro!</p>"; //quando não tiver mais conjuntos ou o arquivo não existir, esta mensagem será exibida
  }
	//fazendo o teste com códigos errados, simplesmente nada acontece. (O que é positivo!)
	?>
	<br><br><a href="/projeto/">Voltar</a>
</body>
</html>
