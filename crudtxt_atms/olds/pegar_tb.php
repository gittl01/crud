<!DOCTYPE html>
<html lang="en">

<head>
    <title>Crud txt em PHP - Lista de registr</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <center>
        <h1>Formulário - Lista de registro</h1>
    </center>

    <?php
    if (file_exists("banco.txt") && !empty(file_get_contents("banco.txt"))) {
        $lista = explode("\n", file_get_contents("banco.txt"));
        #var_dump($lista); #usar essa linha para exibir o conteúdo do vetor
        $conjunto = 1;
        $contador = 0;
        echo "<table>";
        echo "<tr><th>Código</th><th>Nome</th><th>Email</th><th>Data</th><th>Mensagem</th><th>Ações</th>";

        foreach ($lista as $lista_item) {
            #var_dump($lista_item); #usar essa linha para explicar a função explode
            //$coisa = explode($indice, $lista[$tmp2]);
    
            if ("#" == $lista[$contador]) {
                echo "</tr><tr><td>$conjunto</td>";
                $conjunto += 1;
            } else {
                echo "<td>$lista_item</td>";
            }
            $contador += 1;
        }
        echo "</tr></table>";


    } else {
        echo "<br><br><p align=center>Ainda não há nenhum registro!</p>";
    }
    ?>
    <br><br>
    <button onclick="window.location.href = 'index.php'">Cadastrar registro</button>
    <button onclick="window.location.href = 'delete.php'">Deletar registro</button>
    <button onclick="window.location.href = 'select.php'">Atualizar registro</button>

</body>
</form>

</html>