<?php
$banco = "banco.txt";
echo "<CENTER>Registro cadastrados na base de dados.<br></CENTER> ";
echo "<br> ";
if (file_exists($banco) && !empty(file_get_contents($banco))) {
    $lista = explode("\n", file_get_contents($banco));
    unset($lista[count($lista) - 1]); # limpando o espaço fazio do final do conjunto
    $conjunto = 1; //variável para continuar a ordem de aparição do "índice", neste exemplo foi usado o '#'

    echo "<center><table border=1>";
    date_default_timezone_set('America/Sao_Paulo');
    echo "<meta charset='UTF-8'>";

    echo "<tr><th>Código</th><th>Nome</th><th>Email</th><th>Data</th><th>Mensagem</th><th>Ações</th>";

    foreach ($lista as $lista_item) {
        if ("#" == $lista_item) {
            if ($conjunto > 1) { #ajustar a função dos botões, posicionando os botões no final da tabela, sempre depois da primeira linha
                echo "<td><a href='delete.php?codigo=" . $conjunto - 1 . "'><img src='imgs/delete_crud.png' alt='Deletar' title='Deletar registro'></a><a href='monta.php?codigo=" . $conjunto - 1 . "'><img src='imgs/update_crud.png' alt='Atualizar' title='Atualizar registro'><a href='index.php'><img src='imgs/insert_crud.png' alt='Inserir' title='Inserir registro'></td>";
            }
            echo "</tr><tr><td>$conjunto</td>"; #fechamento a linha anterior do conjunto e abre a próxima linha do novo conjunto, incluindo os dados do contador de conjunto
            $conjunto += 1;
        } else {
            echo "<td>$lista_item</td>";
        }
    } # criando os botões de ação da tabela
    echo "<td><a href='delete.php?codigo=" . $conjunto - 1 . "'><img src='imgs/delete_crud.png' alt='Deletar' title='Deletar registro'></a><a href='monta.php?codigo=" . $conjunto - 1 . "'><img src='imgs/update_crud.png' alt='Atualizar' title='Atualizar registro'><a href='index.php'><img src='imgs/insert_crud.png' alt='Inserir' title='Inserir registro'></td></td></table>";
} else {
    echo "<br><br><p align=center>Ainda não há nenhum registro!</p>";
}
?>