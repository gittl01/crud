<?php
if (isset($_GET['codigo'])){
	$banco = "banco.txt";
	$lista = explode("\n", file_get_contents($banco));
	$conjunto = 1; //variável para guardar a ordem de aparição do "índice", neste exemplo foi usado o '#'
	$contador = 0; //variável temporária para manipulação do while e do array $lista
	$lista_itens = count($lista); //gravando quantos itens a lista tinha antes dos unsets

	while ($contador < count($lista)){ //vai percorrer todo o array que foi criado com todas as linhas do arquivo
		if ("#" == $lista[$contador]){ //se achar um '#' no começo da linha, valida se o conjunto é qual o usuário escolheu e acrescenta mais um à variável.
			if ($conjunto == $_GET['codigo']){
				unset($lista[$contador]);
				while ("#" != $lista[$contador] and $contador != $lista_itens) {
					unset($lista[$contador]);
					$contador += 1;
				} //unsets para remover os elementos da lista que foi formada com as linhas do arquivo
			break; // esta linha é equilvalente à um 'break;'
			}
			$conjunto += 1;
		}
		$contador += 1;
	}

	$contador = 0; //variável temporária para manipulação de array e da lista $lista
	$texto = ""; //futuro novo texto que estará no arquivo.
	while ($contador < $lista_itens-1){ //-1 para não colocar line breaks a mais no texto para cada execução
		if (isset($lista[$contador])){ //se não fez parte dos unsets do bloco de código anterior, será atribuído à variável $texto
			$texto .= $lista[$contador] . "\n"; //o elemento válido será juntado à um linebreak na variável
		}
		$contador += 1;
	}
	unlink($banco); //apaga o arquivo do diretório
	$criar = fopen($banco, "a+"); //cria um novo com o mesmo nome já com a permissão de escrita ("a+")
	fwrite($criar, $texto); //escreve no arquivo criado exatamente o que foi colocado na variável $texto
	fclose($criar); //"fecha" o arquivo para o apache //volta para a página de delete sem a atribuição do $_GET na URL;
}
header('Location: /crudtxt_atms/select.php');
//fazendo o teste com códigos errados, simplesmente nada acontece. (O que é positivo!)
?>