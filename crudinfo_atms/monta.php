<!DOCTYPE html>
<html lang="en">

<head>
  <title>Site PHP</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  <?php
  $banco = "banco.txt";
  if (isset($_GET['codigo'])) {
    if (file_exists($banco) && !empty(file_get_contents($banco))) {
      $lista = explode("\n", file_get_contents($banco));
      $informações = 4; //quantidade de informações. Neste caso tem 4: Nome, Email, Data e Mensagem
      $conjunto = $informações*($_GET['codigo']-1); //cálculo para descobrir o conjunto no qual o usuário se refere

      $nome = $lista[$conjunto];
      $email = $lista[$conjunto + 1];
      $data = $lista[$conjunto + 2];
      $mensagem = str_replace("<br>","\n",$lista[$conjunto + 3]); #desfaz a mágica do multiline pra ficar bonitinho no campo

      /* Como fazer comentários no visual Code?
      Shift + Alt + A. */

      }
    }
  ?>
  <form method="post" action="update.php">
    <label for="codigo">Código:</label>
    <input type="number" id="codigo" name="codigo" placeholder="<?php echo $_GET['codigo']; ?>"
      value="<?php echo $_GET['codigo']; ?>" required><br>
    <label for="nome">Nome:</label>
    <input type="text" id="nome" name="nome" placeholder="<?php echo $nome; ?>" value="<?php echo $nome; ?>"
      required><br>
    <label for="email">Email:</label>
    <input type="text" id="email" name="email" placeholder="<?php echo $email; ?>" value="<?php echo $email; ?>"
      required><br>
    <label for="data">Data:</label>
    <input type="text" id="data" name="data" placeholder="<?php echo $data; ?>" value="<?php echo $data; ?>"
      required><br>
    <label for="mensagem">Mensagem:</label>
    <textarea type="text" id="mensagem" name="mensagem" rows="10" cols="40"><?php echo $mensagem; ?></textarea><br>
    <input type="submit" value="Enviar">
  </form>
  <br><br><a href="index.php">Voltar</a>
</body>

</html>