<!DOCTYPE html>
<html lang="en">

<head>
  <title>Site PHP</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  <form method="post" action="update.php">
    <label for="codigo">Código:</label>
    <input type="number" id="codigo" name="codigo" placeholder="<?php echo isset($_GET['codigo']) ? $_GET['codigo'] : ""; ?>" value="<?php echo isset($_GET['codigo']) ? $_GET['codigo'] : ""; ?>" required><br>
    <?php
    require_once('auto.php');
    if (!empty(file_get_contents($banco)) && isset($_GET['codigo'])) {
      $conjunto = $qtdInformacoes*($_GET['codigo']-1);
    }
    for ($i = 0; $i < $qtdInformacoes; $i++){
      echo "<label for='{$informacoes[$i]}'>".ucfirst($informacoes[$i]).":</label>";
      switch ($informacoesTipo[$i]) {
        case 'input':
          echo "<input type='text' id='{$informacoes[$i]}' name='{$informacoes[$i]}' placeholder='" . (isset($conjunto) ? $lista[$conjunto] : "") . "' value='" . (isset($conjunto) ? $lista[$conjunto] : "") . "' required><br>";
          break;
        case 'textarea':
          echo "<textarea type='text' id='{$informacoes[$i]}' name='{$informacoes[$i]}' rows='10' cols='40'>" . (isset($conjunto) ? str_replace("<br>","\n",$lista[$conjunto]) : "") . "</textarea><br>";
          break;
      }
      if (isset($conjunto)){$conjunto++;}
    }
  ?>
    <input type="submit" value="Enviar">
  </form>
  <br><br><a href="select.php">Voltar</a>
</body>

</html>