<!DOCTYPE html>
<html lang='en-US'>
<head>
    <!-- <meta charset="iso-8859-1"> -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Site de Blog!</title>
</head>
<body>
    <div id="body">
	    <div id="header"> </div>
        <div id="sides">
            <div id="left" class="divisions"> </div>
            <div id="middle" class="divisions">
                <form method="post" action="insert.php">
                    <table>
                        <?php
                        require_once('auto.php');
                        for ($i = 0; $i < $qtdInformacoes; $i++){
                            echo "<tr><td><label for='{$informacoes[$i]}'>" . ucfirst($informacoes[$i]) . ":</label></td>";
                            echo "<td><{$informacoesTipo[$i]} id='{$informacoes[$i]}' name='{$informacoes[$i]}' required></{$informacoesTipo[$i]}></td>";
                            echo $i != $qtdInformacoes-1 ? "</tr>" : "<td class='options'><input type='submit' class='formulario' value='Submeter'/></td></tr>";
                        }
                        ?>
                    </table>
                </form>
                    <table>
                        <tr>
                            <td class="options"><button onclick="location.href = '/crudauto_atms/limpar.php'">Limpar Banco!</button></td>
                            <td class="options"><button onclick="location.href = '/crudauto_atms/reset.php'">Resetar Banco!</button></td>
                            <td class="options"><button onclick="location.href = '/crudauto_atms/select.php'">Selecionar</button></td>
                        </tr>
                    </table>
</body>