<?php
require_once('auto.php');
echo "<CENTER>Registro cadastrados na base de dados.<br></CENTER> ";
echo "<br> ";
if (!empty(file_get_contents($banco))) {
    echo "<center><table border=1>";
    date_default_timezone_set('America/Sao_Paulo');
    echo "<meta charset='UTF-8'>";

    echo "<tr><th>Código</th>";
    foreach($informacoes as $item){ echo "<th>" . ucfirst($item) . "</th>"; }
    echo "<th>Ações</th></tr>";

    $codigo = 1;
    for ($i = 0; $i < count($lista)-1; $i+=$qtdInformacoes){
        echo "<tr>";
        echo "<td id='$codigo'>$codigo</td>";

        for ($j=$i; $j < $i+$qtdInformacoes; $j++){
            echo "<td>{$lista[$j]}</td>";
        }

        echo "<td><a href='delete.php?codigo=$codigo'><img src='imgs/delete_crud.png' alt='Deletar' title='Deletar registro'></a><a href='monta.php?codigo=$codigo'><img src='imgs/update_crud.png' alt='Atualizar' title='Atualizar registro'><a href='index.php'><img src='imgs/insert_crud.png' alt='Inserir' title='Inserir registro'></td>";
        echo "</tr>";

        $codigo++;
    }
    echo "</table>";
    echo "<footer id='footer'></footer>";
} else {
    echo "<br><br><p align=center>Ainda não há nenhum registro!</p>";
}
?>
